# UX Playground

![UX Playground Image](./public_additional/android-chrome-192x192.png)

You can view the Playground in [StorybookJS](https://storybook.js.org) by visiting the Gitlab page: https://borderux.gitlab.io/uxplayground

*NOTE*: It can take a while to load because Gitlab pages is slow

## Background
UX Playground is a testing ground and research for different public Design Systems to gain understanding 
of the design system approach and which tools can be used for different needs.  It also should give UX
designers an understanding of the different packages that exist and terminology and styling that are
currently being used in the industry.  

## UX Design Systems
* [Material UI](./src/materialui)
* [Ant Design](./src/antd)
* [Semantic UI](./src/semanticui)
* [Chakra UI](./src/chakra)
* [Adobe Spectrum](./src/spectrum)