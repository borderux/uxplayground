const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;

async function webpackFinal(config, { configType }) {
  if (configType !== 'PRODUCTION') {
    return config;
  }
  config.plugins.push(
    new BundleAnalyzerPlugin({
      reportFilename: 'report.html',
      openAnalyzer: false,
      analyzerMode: 'static',
    })
  );
  return config;
}

async function babel(options) {
  const ops = { ...options };
  ops.plugins.push(require.resolve('babel-plugin-styled-components'));
  return ops;
}

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/preset-create-react-app',
  ],
  webpackFinal: webpackFinal,
  babel: babel,
};
