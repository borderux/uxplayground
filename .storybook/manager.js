import { addons } from '@storybook/addons';
import { create } from '@storybook/theming/create';

const theme = create({
  base: 'light',
  brandTitle: 'UX Playground',
  brandUrl: 'https://borderux.com',
  brandImage: 'https://demo.borderux.com/border-logo.png',
});

addons.setConfig({
  theme,
});
