import React from 'react';
import { Button as AntDButton } from 'antd';
import { ButtonProps } from 'antd/es/button';
import 'antd/dist/antd.css';

// https://ant.design/components/button/#API

export const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
  return <AntDButton {...props} />;
};
