import React from 'react';
import { Button } from './Button';
import { StyledButton } from './StyledButton';
import { ReactComponent as LoaderIcon } from '../../assets/loader.svg';

export default {
  title: 'Ant Design/Button',
  component: Button,
};

export const normal = () => <Button>Normal Button</Button>;

export const primary = () => <Button type={'primary'}>Primary Button</Button>;

export const ghost = () => (
  <Button type={'ghost'} ghost>
    Ghost Button
  </Button>
);

export const dashed = () => <Button type={'dashed'}>Dashed Button</Button>;

export const link = () => <Button type={'link'}>Link Button</Button>;

export const text = () => <Button type={'text'}>Text Button</Button>;

export const loading = () => <Button loading>Loading Button</Button>;

export const icon = () => <Button icon={<LoaderIcon />}>Icon Button</Button>;

export const circle = () => <Button shape={'circle'}>Circle Button</Button>;

export const round = () => <Button shape={'round'}>Round Button</Button>;

export const large = () => <Button size={'large'}>Large Button</Button>;

export const middle = () => <Button size={'middle'}>Middle Button</Button>;

export const small = () => <Button size={'small'}>Small Button</Button>;

export const ghostTransparent = () => (
  <Button ghost>Ghost Transparent Button</Button>
);

export const disabled = () => <Button disabled>Disabled Button</Button>;

export const danger = () => <Button danger>Danger Button</Button>;

export const htmlButton = () => (
  <Button href={'https://ant.design/components/button/#API'}>
    HTML Button
  </Button>
);

export const styledRedButton = () => (
  <StyledButton>Styled Components Button</StyledButton>
);
