import React from 'react';
import { Button as AntDButton } from 'antd';
import { ButtonProps } from 'antd/es/button';
import styled from 'styled-components';
import 'antd/dist/antd.css';

// https://ant.design/components/button/#API

const StyledAntButton = styled(AntDButton)`
  background: red;
  color: white;
  border-radius: 8px;
  &:hover {
    background: purple;
  }
`;

export const StyledButton: React.FC<ButtonProps> = (props: ButtonProps) => {
  return <StyledAntButton {...props} />;
};
