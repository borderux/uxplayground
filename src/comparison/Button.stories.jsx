import React from 'react';
import { VerticalList } from '../common/VerticalList/VerticalList';
import { Button as AntDButton } from '../antd/Button/Button';
import { Button as MaterialUIButton } from '../materialui/Button/Button';

export default {
  title: 'Comparison/Button',
};

const ITEMS = [
  {
    component: <AntDButton>Ant Design Button</AntDButton>,
    title: 'Ant Design Button',
    link: 'Ant Design/Button',
    description: 'Ant design button',
  },
  {
    component: <MaterialUIButton>MaterialUI Button</MaterialUIButton>,
    title: 'Material UI Button',
    link: 'MaterialUI/Button',
    description: 'Material UI button',
  },
];

export const button = () => (
  <VerticalList
    items={ITEMS}
    componentWidth={'200px'}
    componentHeight={'100px'}
  />
);
