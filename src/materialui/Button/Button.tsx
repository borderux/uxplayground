import React from 'react';
import MaterialButton, { ButtonProps } from '@material-ui/core/Button';

export const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
  return <MaterialButton {...props} />;
};
