import React from 'react';
import { Button } from './Button';
import { StyledButton } from './StyledButton';
import { MaterialStyledButton } from './MaterialStyledButton';
import { ReactComponent as LoaderIcon } from '../../assets/loader.svg';

export default {
  title: 'MaterialUI/Button',
  component: Button,
};

export const normal = () => <Button>Normal Button</Button>;

export const primary = () => <Button color={'primary'}>Primary Button</Button>;

export const secondary = () => (
  <Button color={'secondary'}>Secondary Button</Button>
);

export const contained = () => (
  <Button variant={'contained'}>Contained Button</Button>
);

export const containedPrimary = () => (
  <Button variant={'contained'} color={'primary'}>
    Contained Button
  </Button>
);

export const containedSecondary = () => (
  <Button variant={'contained'} color={'secondary'}>
    Contained Button
  </Button>
);

export const outlined = () => (
  <Button variant={'outlined'}>Outlined Button</Button>
);

export const outlinedPrimary = () => (
  <Button variant={'outlined'} color={'primary'}>
    Outlined Primary
  </Button>
);

export const outlinedSecondary = () => (
  <Button variant={'outlined'} color={'secondary'}>
    Outlined Secondary
  </Button>
);

export const large = () => <Button size={'large'}>Large Button</Button>;

export const medium = () => <Button size={'medium'}>Medium Button</Button>;

export const small = () => <Button size={'small'}>Small Button</Button>;

export const disabled = () => <Button disabled>Disabled Button</Button>;

export const disabledContained = () => (
  <Button variant={'contained'} disabled>
    Disabled Contained
  </Button>
);

export const styledButton = () => (
  <StyledButton>Styled Components Button</StyledButton>
);

export const styledMaterialButton = () => (
  <MaterialStyledButton>Material Styled Button</MaterialStyledButton>
);

export const iconButton = () => (
  <Button startIcon={<LoaderIcon />}>Icon Button</Button>
);
