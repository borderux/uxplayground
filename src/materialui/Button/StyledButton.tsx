import React from 'react';
import styled from 'styled-components';
import MaterialButton, { ButtonProps } from '@material-ui/core/Button';

// When styling with styled-components, in order to increase the specificity of the
// styles set, you can use the && trick as shown below.  The other option is to use the
// injectFirst option the <StylesProvider> component provided by Material UI
// https://material-ui.com/styles/advanced/#injectfirst
const StyledMaterialButton = styled(MaterialButton)`
  && {
    background: red;
    border-radius: 0;
    color: white;
    &:hover {
      background: purple;
    }
  }
`;

export const StyledButton: React.FC<ButtonProps> = (props: ButtonProps) => {
  return <StyledMaterialButton {...props} />;
};
