import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaterialButton, { ButtonProps } from '@material-ui/core/Button';

// You can use the makeStyles JSS-in-CSS solution to override the styling for the button
// If you want to use direct CSS as a string (similar to styled-components), you can use the
// jss-plugin-template as described here
// https://material-ui.com/styles/advanced/#string-templates
const useStyles = makeStyles({
  root: {
    background: 'blue',
    borderRadius: 0,
    color: 'white',
    '&:hover': {
      background: 'purple',
    },
  },
});

export const MaterialStyledButton: React.FC<ButtonProps> = (
  props: ButtonProps
) => {
  const classes = useStyles();
  return <MaterialButton {...props} className={classes.root} />;
};
