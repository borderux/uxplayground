import React from 'react';
import LinkTo from '@storybook/addon-links/react';
import './styles.css';

export interface VerticalListItem {
  component: any;
  title: string;
  description?: string;
  link: string;
}

export interface VerticalListProps {
  items: Array<VerticalListItem>;
  componentWidth?: string;
  componentHeight?: string;
}

export const VerticalList: React.FC<VerticalListProps> = ({
  items,
  componentWidth,
  componentHeight,
}) => {
  if (!items || !items.length) {
    return null;
  }
  const renderItem = (item: VerticalListItem) => {
    return (
      <li className={'VerticalListItem'} key={item.title}>
        <div
          style={{
            minWidth: componentWidth || undefined,
            minHeight: componentHeight || undefined,
          }}
          className={'VerticalListComponent'}
        >
          {item.component}
        </div>
        <div className={'VerticalListInfo'}>
          <LinkTo story={item.link}>
            <span className={'VerticalListTitle'}>{item.title}</span>
          </LinkTo>
          <span className={'VerticalListDescription'}>{item.description}</span>
        </div>
      </li>
    );
  };
  return <ul className={'VerticalList'}>{items.map(renderItem)}</ul>;
};
