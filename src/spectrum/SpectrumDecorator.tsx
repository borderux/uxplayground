import React from 'react';
import { Provider, defaultTheme } from '@adobe/react-spectrum';

export const SpectrumDecorator = (Story: any) => {
  return <Provider theme={defaultTheme}>{<Story />}</Provider>;
};
