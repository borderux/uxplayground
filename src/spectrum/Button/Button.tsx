import React from 'react';
import { Button as SpectrumButton } from '@adobe/react-spectrum';
import { SpectrumButtonProps } from '@react-types/button';

export const Button: React.FC<SpectrumButtonProps> = (
  props: SpectrumButtonProps
) => {
  return <SpectrumButton {...props} />;
};
