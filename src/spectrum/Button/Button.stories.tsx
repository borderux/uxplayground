import React from 'react';
import { SpectrumDecorator } from '../SpectrumDecorator';
import { Button } from './Button';

export default {
  title: 'Spectrum/Button',
  component: Button,
  decorators: [SpectrumDecorator],
};

export const normal = () => <Button variant={'cta'}>Normal Button</Button>;
