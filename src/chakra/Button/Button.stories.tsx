import React from 'react';
import { Button } from './Button';
import { ChakraDecorator } from '../ChakraDecorator';
// import { StyledButton } from './StyledButton';
// import { MaterialStyledButton } from './MaterialStyledButton';
// import { ReactComponent as LoaderIcon } from '../../assets/loader.svg';

export default {
  title: 'ChakraUI/Button',
  component: Button,
  decorators: [ChakraDecorator],
};

export const normal = () => <Button>Normal Button</Button>;

export const outline = () => (
  <Button variant={'outline'}>Outline Button</Button>
);

export const ghost = () => <Button variant={'ghost'}>Ghost Button</Button>;

export const disabled = () => <Button isDisabled>Disabled Button</Button>;

export const loading = () => <Button isLoading>Loading Button</Button>;

export const loadingText = () => (
  <Button isLoading loadingText={'Loading Text'}>
    Loading Button
  </Button>
);

export const large = () => <Button size={'lg'}>Large Button</Button>;

export const medium = () => <Button size={'md'}>Medium Button</Button>;

export const small = () => <Button size={'sm'}>Small Button</Button>;

export const xtraSmall = () => <Button size={'xs'}>Xtra Small Button</Button>;
