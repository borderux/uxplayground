import React from 'react';
import { Button as ChakraButton, IButton } from '@chakra-ui/core';

export const Button: React.FC<IButton> = (props: IButton) => {
  return <ChakraButton {...props} />;
};
