import React from 'react';
import { theme, ThemeProvider } from '@chakra-ui/core';

export const ChakraDecorator = (Story: any) => {
  return <ThemeProvider theme={theme}>{<Story />}</ThemeProvider>;
};
