import React from 'react';
import { Button as SemanticButton, ButtonProps } from 'semantic-ui-react';
// You need to directly import the components default CSS to get default stylings, or just define your own
import 'semantic-ui-css/components/button.min.css';

export const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
  return <SemanticButton {...props} />;
};
