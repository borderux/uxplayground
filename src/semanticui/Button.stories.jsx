import React from 'react';
import { Button } from './Button';
// import { ReactComponent as LoaderIcon } from '../assets/loader.svg';

export default {
  title: 'SemanticUI/Button',
  component: Button,
};

export const normal = () => <Button>Normal Button</Button>;

export const primary = () => <Button primary>Primary Button</Button>;

export const secondary = () => <Button secondary>Secondary Button</Button>;

export const active = () => <Button active>Active Button</Button>;

export const basic = () => <Button basic>Basic Button</Button>;

export const circular = () => <Button circular>Circular Button</Button>;

export const compact = () => <Button compact>Compact Button</Button>;

export const disabled = () => <Button disabled>Disabled Button</Button>;

export const loading = () => <Button loading>Loading Button</Button>;

export const negative = () => <Button negative>Negative Button</Button>;

export const positive = () => <Button positive>Positive Button</Button>;

export const massive = () => <Button size={'massive'}>Massive Button</Button>;

export const huge = () => <Button size={'huge'}>Huge Button</Button>;

export const big = () => <Button size={'big'}>Big Button</Button>;

export const large = () => <Button size={'large'}>Large Button</Button>;

export const medium = () => <Button size={'medium'}>Medium Button</Button>;

export const small = () => <Button size={'medium'}>Small Button</Button>;

export const tiny = () => <Button size={'tiny'}>Tiny Button</Button>;

export const mini = () => <Button size={'mini'}>Mini Button</Button>;

export const animatedFade = () => (
  <Button animated={'fade'}>Animated Fade</Button>
);

export const animatedVertical = () => (
  <Button animated={'vertical'}>Animated Vertical</Button>
);
